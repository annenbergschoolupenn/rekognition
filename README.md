# AWS Rekognition - ASC DATASCIENCE
## Etienne P Jacquot - 07/11/2020


### Image & Video Analysis

- For more information on AWS offering visit [AWS REKOGNITION](https://aws.amazon.com/rekognition/)

- This repo contains python code & examples for ASC grad student image analysis data requests, a work in progress.


### Directories:

- [notebook](./notebooks): python notebooks with instructions on getting AWS Rekognition results
    - Specifically, this project is lead by ASC Grad Student ILangrock who requested rekognition results for US Democratic Candidate Instagram posts, over 11,000 images in total.
- [data](./data): source instagram data with URLs (most likely on https://imghost.asc.upenn.edu or in an S3 bucket), downloaded as .CSV from AWS Athean results via SQL query
- [rekog_output](./rekog_output): outdir for `pd.to_json()` for instagram dataframe with rekognition column (nested json object w/ rekognition results for that image).

____________

